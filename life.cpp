#include "life.h"
#include <QFile>
#include <QVariant>
#include <QStringList>
#include <QCoreApplication>

Life::Life():QObject(nullptr),
    turnCount(0),
    turnTimeoutMs(0),
    currentTurn(0),
    printEveryTurn(false)
{
    connect( &timer,SIGNAL(timeout()),this,SLOT(processTimeout()) );
}

bool Life::initByConfFile(const QString &fileName)
{

     QFile file(fileName);

     if( !file.open(QFile::ReadOnly) )
     {
         qDebug() <<Q_FUNC_INFO<<"Error! Can't open config file!";
         return false;
     }

     QTextStream in(&file);

     bool inMapSection=false;

     while (!in.atEnd())
     {
         QString line = in.readLine();
         if(line.contains("turns"))
         {
             turnCount=line.split("=").value(1).toInt();
         }else
         if(line.contains("turnTimeoutMs"))
         {
             turnTimeoutMs=line.split("=").value(1).toInt();

         }else
         if(line.contains("printEveryTurn"))
         {
             printEveryTurn= ( line.split("=").value(1)=="true" || line.split("=").value(1)=="1" ) ? true : false;

         }else
         if( inMapSection )
         {

            QString strRow=line.simplified().trimmed();
            if(strRow.contains("}"))
            {
                inMapSection=false;
                continue;
            }

            QList<Cell> row;
            foreach (QChar ch, strRow)
            {
                row<<(Cell)ch.digitValue();
            }
            field<<row;

         }else
         if(line.contains("map"))
         {
             inMapSection=true;
         }
     }


     if( turnCount>0 && field.size() > 0  && turnTimeoutMs >=0)
     {
         return true;
     }
     return false;
}

void Life::start()
{
    timer.start(turnTimeoutMs);
}

void  Life::print()
{
    qDebug()<<Q_FUNC_INFO<<"map= { ";

    foreach (auto row, field)
    {
        QString strRow;
        foreach (const Cell& cell, row)
        {
            if(cell==Cell::EMPTY)
                strRow+="0";
            else
                strRow+="1";
        }

        qDebug()<<strRow;
    }

    qDebug()<<Q_FUNC_INFO<<"} ";

}
void Life::processTimeout()
{
    qDebug()<<Q_FUNC_INFO<<"step N="<<currentTurn;
    if(currentTurn==turnCount)
    {
        timer.stop();
        qDebug()<<Q_FUNC_INFO<<"\n\nDONE!!!";
        print();
        QCoreApplication::quit();
        return;
    }
    currentTurn++;
    makeStep();
    if(printEveryTurn)
        print();
}

void Life::makeStep()
{

    for(int x=0;x<field.size(); ++x )
    {
        for(int y=0; y< field.value(x).size(); ++y )
        {
            int roundLifeCount=0;

            roundLifeCount= ( field.value(x)  .value(y+1) == Cell::CREATURE )  ? (roundLifeCount+1):roundLifeCount ;
            roundLifeCount= ( field.value(x)  .value(y-1) == Cell::CREATURE )  ? (roundLifeCount+1):roundLifeCount ;
            roundLifeCount= ( field.value(x+1).value(y+1) == Cell::CREATURE )  ? (roundLifeCount+1):roundLifeCount ;
            roundLifeCount= ( field.value(x+1).value(y)   == Cell::CREATURE )  ? (roundLifeCount+1):roundLifeCount ;
            roundLifeCount= ( field.value(x+1).value(y-1) == Cell::CREATURE )  ? (roundLifeCount+1):roundLifeCount ;
            roundLifeCount= ( field.value(x-1).value(y+1) == Cell::CREATURE )  ? (roundLifeCount+1):roundLifeCount ;
            roundLifeCount= ( field.value(x-1).value(y)   == Cell::CREATURE )  ? (roundLifeCount+1):roundLifeCount ;
            roundLifeCount= ( field.value(x-1).value(y-1) == Cell::CREATURE )  ? (roundLifeCount+1):roundLifeCount ;

            if( field.at(x).at(y) == Cell::EMPTY  && roundLifeCount ==3 )
            {
                field[x][y]=Cell::CREATURE;
            }else
            if(    field.at(x).at(y) == Cell::CREATURE
                   && ( roundLifeCount ==3 || roundLifeCount ==2) )
            {
                continue;
            }else
            {
                field[x][y]=Cell::EMPTY;
            }
        }
    }

}

