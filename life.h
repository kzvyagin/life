#ifndef LIFE_H
#define LIFE_H

#include <QList>
#include <QDebug>
#include <QString>
#include <QTextStream>
#include <QTimer>
#include <QObject>


enum Cell
{
    EMPTY=0,
    CREATURE=1
};

class Life :public QObject
{
    Q_OBJECT
public:
    Life();

    bool initByConfFile(const QString &fileName);
    void start();
    void print();

private slots:
    void processTimeout();
private:
    void makeStep();


    int turnCount;
    int turnTimeoutMs;
    int currentTurn;
    bool printEveryTurn;
    QList< QList < Cell > > field;
    QTimer timer;
};

#endif // LIFE_H
