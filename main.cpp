#include <QCoreApplication>
#include "life.h"
#include <QDebug>
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString filename=a.arguments().value(1);


    Life life;
    bool ok=life.initByConfFile(filename);

    if(!ok)
    {
        qDebug()<<Q_FUNC_INFO<<"Can't start life simulator";
        return -1;

    }
    life.print();
    life.start();
    return a.exec();
}
